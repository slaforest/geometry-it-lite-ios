//
//  ToolsListViewController.m
//  GeomeTry-It
//
//  Created by Scott LaForest on 5/7/13.
//  Copyright (c) 2013 Zoo Town Tech. All rights reserved.
//

#import "ToolsListViewController.h"
#import "DetailViewController.h"
#import "AppDelegate.h"
#import "ProtractorViewController.h"
#import "GeoTryItIAPHelper.h"
#import "PythagoreanViewController.h"
#import "RightTriangleCheckerViewController.h"

@interface ToolsListViewController ()

@end

@implementation ToolsListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Tools", @"Tools");
        if ([ADBannerView instancesRespondToSelector:@selector(initWithAdType:)]) {
            _adBannerView = [[ADBannerView alloc] initWithAdType:ADAdTypeBanner];
        } else {
            
            _adBannerView = [[ADBannerView alloc] init];
        }
        _adBannerView.frame = CGRectMake(0, self.view.frame.size.height, _adBannerView.frame.size.width, _adBannerView.frame.size.height);
        _adBannerView.delegate = self;
    }
    return self;
}
-(void) viewWillAppear:(BOOL)animated{
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"geometryItLite_b2school2013"]) {
        if (![mutToolsList containsObject:@"Protractor"]) {
            [mutToolsList addObject:@"Protractor"];
            [mutToolsList addObject:@"Pythagorean Theorem"];
            [mutToolsList addObject:@"Right Triangle Checker"];
        }
        
    }
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"geometryItLite_NoAds"]) {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            if (_adBannerView) {
                [_adBannerView removeFromSuperview];
            }
        }
        else {
            AppDelegate* appDelgate = [[UIApplication sharedApplication]delegate];
            if (appDelgate.window.rootViewController.class != appDelgate.splitViewController.class) {
                [appDelgate setUpMainScreen];
                [_adBannerView removeFromSuperview];

            }
        }
        
    }
    
    toolsList = [mutToolsList copy];
    [self.tableView reloadData];
    NSLog(@"tools %@, %@", toolsList, mutToolsList);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"View Did load");
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [self.view addSubview:_adBannerView];
    }
   // NSSet* prodIDs = [[GeoTryItIAPHelper sharedInstance] getProductIDs];
   // NSMutableArray* myMutArray = [[NSMutableArray alloc]init];
    mutToolsList = [[NSMutableArray alloc]initWithObjects:@"3D Shapes", @"Transformations",nil];
    /*for (NSString* prodId in prodIDs) {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:prodId])  {
            [myMutArray addObject:prodId];
        }
    }*/
    
    /*for (int i = 0; i < myMutArray.count; i++) {
        if ([myMutArray[i] isEqual:@"geometryItLite_b2school2013"]) {
            [mutToolsList addObject:@"Protractor"];
            [mutToolsList addObject:@"Pythagorean Theorem"];
            [mutToolsList addObject:@"Right Triangle Checker"];
        }
    }*/
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return toolsList.count  ;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.text = toolsList[indexPath.row];
    cell.textLabel.textColor = [UIColor colorWithRed:0 green:0.533 blue:1.0 alpha:1.0];
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    NSString* urlString = @"";
    if (indexPath.row == 0){
        urlString = @"http://www.geometry-it.appspot.com/3d_main";
    }else if (indexPath.row == 1){
        urlString = @"http://www.geometry-it.appspot.com/transformations";
    }
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
	    
            if (!self.detailViewController) {
	        self.detailViewController = [[DetailViewController alloc] initWithNibName:@"DetailViewController_iPhone" bundle:nil];
            }
        if (indexPath.row == 2) {
            ProtractorViewController* protractorVC = [[ProtractorViewController alloc] initWithNibName:@"ProtractorViewController" bundle:nil];
            protractorVC.navigationItem.title = toolsList[indexPath.row];
            [self.navigationController pushViewController:protractorVC animated:YES];
        }else if (indexPath.row == 3){
            PythagoreanViewController* pvc = [[PythagoreanViewController alloc]initWithNibName:@"PythagoreanViewController" bundle:nil];
            pvc.navigationItem.title = toolsList[indexPath.row];
            [self.navigationController pushViewController:pvc animated:YES];
        }else if (indexPath.row == 4){
            RightTriangleCheckerViewController* pvc = [[RightTriangleCheckerViewController alloc]initWithNibName:@"RightTriangleCheckerViewController" bundle:nil];
            pvc.navigationItem.title = toolsList[indexPath.row];
            [self.navigationController pushViewController:pvc animated:YES];

        }
        else{
            self.detailViewController.navigationItem.title = toolsList[indexPath.row];
            [self.navigationController pushViewController:self.detailViewController animated:YES];
        }
    }
    else {
        AppDelegate* appDelgate = [[UIApplication sharedApplication]delegate];
        [appDelgate.splitViewController viewWillDisappear:YES];
        UIBarButtonItem* popoverButton = [[[[self.splitViewController.viewControllers objectAtIndex:1]topViewController]navigationItem ]leftBarButtonItem];
        NSMutableArray* viewControllerArray = [[NSMutableArray alloc]initWithArray:appDelgate.splitViewController.viewControllers];
        [viewControllerArray removeLastObject];
        
        
        if (indexPath.row == 0 || indexPath.row == 1) {
            DetailViewController* detailVC = [[DetailViewController alloc]initWithNibName:@"DetailViewController_iPad" bundle:nil];
            UINavigationController *detailNavigationController = [[UINavigationController alloc] initWithRootViewController:detailVC];
            //detailNavigationController.navigationBar.tintColor = [UIColor blackColor];
            [viewControllerArray addObject:detailNavigationController];
            appDelgate.splitViewController.delegate = detailVC;
            detailVC.navigationItem.leftBarButtonItem = popoverButton;
            self.detailViewController = detailVC;
            detailVC.navigationItem.title =  toolsList[indexPath.row];
            detailVC.urlstring = urlString;
        
        }else if (indexPath.row == 2){
            ProtractorViewController* protractorVC = [[ProtractorViewController alloc]initWithNibName:@"ProtractorViewController_iPad" bundle:nil];
            UINavigationController *detailNavigationController = [[UINavigationController alloc] initWithRootViewController:protractorVC];
            protractorVC.navigationItem.leftBarButtonItem = popoverButton;
            //detailNavigationController.navigationBar.tintColor = [UIColor blackColor];
            [viewControllerArray addObject:detailNavigationController];
            appDelgate.splitViewController.delegate= protractorVC;
            protractorVC.navigationItem.title = toolsList[indexPath.row];
        }
        else if (indexPath.row == 3){
        PythagoreanViewController* pvc = [[PythagoreanViewController alloc]initWithNibName:@"PythagoreanViewController_iPad" bundle:nil];
        UINavigationController *detailNavigationController = [[UINavigationController alloc] initWithRootViewController:pvc];
        pvc.navigationItem.leftBarButtonItem = popoverButton;
        //detailNavigationController.navigationBar.tintColor = [UIColor blackColor];
        [viewControllerArray addObject:detailNavigationController];
        appDelgate.splitViewController.delegate= pvc;
        pvc.navigationItem.title = toolsList[indexPath.row];
        }
        else if (indexPath.row == 4){
        RightTriangleCheckerViewController* rtvc = [[RightTriangleCheckerViewController alloc]initWithNibName:@"RightTriangleCheckerController_iPad" bundle:nil];
        UINavigationController *detailNavigationController = [[UINavigationController alloc] initWithRootViewController:rtvc];
        rtvc.navigationItem.leftBarButtonItem = popoverButton;
        //detailNavigationController.navigationBar.tintColor = [UIColor blackColor];
        [viewControllerArray addObject:detailNavigationController];
        appDelgate.splitViewController.delegate= rtvc;
        rtvc.navigationItem.title = toolsList[indexPath.row];
        }


        [appDelgate.splitViewController setViewControllers:viewControllerArray];
        [appDelgate.splitViewController viewWillAppear:YES];
    
    }
        NSURLRequest* request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        [self.detailViewController.webView loadRequest:request];

}
- (void)layoutAnimated:(BOOL)animated
{
    // As of iOS 6.0, the banner will automatically resize itself based on its width.
    // To support iOS 5.0 however, we continue to set the currentContentSizeIdentifier appropriately.
    CGRect contentFrame = self.view.bounds;
    if (contentFrame.size.width < contentFrame.size.height) {
        //_adBannerView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierPortrait;
    } else {
       // _adBannerView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierLandscape;
    }
    
    CGRect bannerFrame = _adBannerView.frame;
    if (_adBannerView.bannerLoaded) {
        contentFrame.size.height -= _adBannerView.frame.size.height;
        bannerFrame.origin.y = contentFrame.size.height;
    } else {
        bannerFrame.origin.y = contentFrame.size.height;
    }
    
    [UIView animateWithDuration:animated ? 0.25 : 0.0 animations:^{
        _contentView.frame = contentFrame;
        [_contentView layoutIfNeeded];
        _adBannerView.frame = bannerFrame;
    }];
}

#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_6_0
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}
#endif

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

- (void)viewDidLayoutSubviews
{
    [self layoutAnimated:[UIView areAnimationsEnabled]];
}

- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    
    [self layoutAnimated:YES];
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    [self layoutAnimated:YES];
}

- (BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave
{
    return YES;
}

- (void)bannerViewActionDidFinish:(ADBannerView *)banner
{
}

@end
