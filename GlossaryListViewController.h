//
//  GlossaryListViewController.h
//  GeomeTry-It
//
//  Created by Scott LaForest on 5/7/13.
//  Copyright (c) 2013 Zoo Town Tech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iAd/iAd.h>

@class DetailViewController;
@interface GlossaryListViewController : UIViewController<UITableViewDataSource,UITableViewDelegate, UISearchBarDelegate, UIAlertViewDelegate, ADBannerViewDelegate>{
    NSArray *termsList;
    NSArray *jsonArray;
    NSArray* lettersNeeded;
    NSArray* arrangedTerms;
    NSArray *searchedData;
    NSDate* lastJSONGet;
    BOOL isFiltered;
    
}

@property (strong, nonatomic) DetailViewController *detailViewController;
@property (nonatomic, retain) IBOutlet UITableView *tableView;
@property (nonatomic, retain) IBOutlet UIView *contentView;
@property (nonatomic, retain) ADBannerView* adBannerView;
@property (nonatomic) BOOL adBannerViewIsVisible;
@property(strong , nonatomic) IBOutlet UISearchBar* searchBar;
@property(strong , nonatomic) IBOutlet UISearchDisplayController* searchDisplay;

@end
