//
//  SimpleTableCell.h
//  GeomeTryItLite
//
//  Created by Scott LaForest on 7/18/13.
//  Copyright (c) 2013 Zoo Town Tech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SimpleTableCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *descriptionLabel;
@property (nonatomic, weak) IBOutlet UILabel *priceLabel;

@end
