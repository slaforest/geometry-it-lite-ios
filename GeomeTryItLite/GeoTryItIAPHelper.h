//
//  GeoTryItIAPHelper.h
//  GeomeTryItLite
//
//  Created by Scott LaForest on 7/18/13.
//  Copyright (c) 2013 Zoo Town Tech. All rights reserved.
//

#import "IAPHelper.h"

@interface GeoTryItIAPHelper : IAPHelper

+(GeoTryItIAPHelper*)sharedInstance;

@end
