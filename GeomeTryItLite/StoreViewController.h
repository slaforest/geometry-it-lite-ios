//
//  StoreViewController.h
//  GeomeTryItLite
//
//  Created by Scott LaForest on 7/18/13.
//  Copyright (c) 2013 Zoo Town Tech. All rights reserved.
//

#import <UIKit/UIKit.h>
@class DetailViewController;

@interface StoreViewController : UITableViewController{
    NSArray* _products;
}

    @property (strong, nonatomic) DetailViewController *detailViewController;

@end
