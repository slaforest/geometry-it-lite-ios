//
//  StoreDetailViewController.h
//  GeomeTryItLite
//
//  Created by Scott LaForest on 7/20/13.
//  Copyright (c) 2013 Zoo Town Tech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>

@interface StoreDetailViewController : UITableViewController
@property (strong, nonatomic) SKProduct* product;

@end
