//
//  GeoTryItIAPHelper.m
//  GeomeTryItLite
//
//  Created by Scott LaForest on 7/18/13.
//  Copyright (c) 2013 Zoo Town Tech. All rights reserved.
//

#import "GeoTryItIAPHelper.h"

@implementation GeoTryItIAPHelper

+ (GeoTryItIAPHelper *)sharedInstance {
    static dispatch_once_t once;
    static GeoTryItIAPHelper * sharedInstance;
    dispatch_once(&once, ^{
        NSSet * productIdentifiers = [NSSet setWithObjects:
                                      @"geometryItLite_b2school2013",
                                      @"geometryItLite_NoAds",
                                      nil];
        sharedInstance = [[self alloc] initWithProductIdentifiers:productIdentifiers];
    });
    return sharedInstance;
}


@end
