//
//  ProtractorViewController.m
//  GeomeTry-It
//
//  Created by Scott LaForest on 6/5/13.
//  Copyright (c) 2013 Zoo Town Tech. All rights reserved.
//

#import "ProtractorViewController.h"

@interface ProtractorViewController ()

@end

@implementation ProtractorViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void) viewWillAppear:(BOOL)animated   {
    //UIImage* img = [_photoView image];
    //[_photoView setImage:[self imageWithImage:img scaledToSize:self.photoView.bounds.size]];

}

-(UIImage*)imageWithImage: (UIImage*) image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.picker = [[UIImagePickerController alloc] init];
    self.picker.delegate = self;
    self.picker.navigationBarHidden = YES;
    self.picker.toolbarHidden = YES;
    self.picker.wantsFullScreenLayout = YES;
    touchesCounter = 0;
    touchesHolder = [[NSMutableArray alloc]initWithCapacity:3];
    //[_photoView setImage:[UIImage imageNamed:@"acute_angle.png"]];
    self.photoView.translatesAutoresizingMaskIntoConstraints = NO;
    UITapGestureRecognizer * recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    recognizer.delegate = self;
    [_scrollView addGestureRecognizer:recognizer];
    UIBarButtonItem* cameraBtn = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCamera target:self action:@selector(showCamera)];
    //self.navigationItem.rightBarButtonItem = cameraBtn;
    UIButton *infoBtn = [UIButton buttonWithType:UIButtonTypeInfoLight];
    UIBarButtonItem* infoBarBtn = [[UIBarButtonItem alloc] initWithCustomView:infoBtn];
    [infoBtn addTarget:self action:@selector(showInfo) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItems = [[NSArray alloc]initWithObjects:cameraBtn,infoBarBtn, nil];
    
}

- (void)handleTap:(UITapGestureRecognizer *)sender {
    if (sender.state == UIGestureRecognizerStateEnded)     {
       CGPoint currentPoint = [sender locationInView:_photoView];
        if (touchesCounter == 0) {
            ptA = CGPointMake(0.0, 0.0);
            vertex = CGPointMake(0.0, 0.0);
            ptB = CGPointMake(0.0, 0.0);
            self.photoView.layer.sublayers = nil;
            [self.degreeVal setHidden:YES];
            [self.radVal setHidden:YES];
            [self.conjBtn setHidden:YES];
            ptA = currentPoint;
        }else if(touchesCounter == 1){
            vertex  = currentPoint;
        }else if(touchesCounter == 2){
            ptB  =currentPoint;
        }
        
        CAShapeLayer *circleLayer = [CAShapeLayer layer];
        // Give the layer the same bounds as your image view
        [circleLayer setBounds:CGRectMake(0.0f, 0.0f, [_photoView bounds].size.width,
                                          [_photoView bounds].size.height)];
        // Position the circle anywhere you like, but this will center it
        // In the parent layer, which will be your image view's root layer
        [circleLayer setPosition:CGPointMake([_photoView bounds].size.width/2.0f,
                                             [_photoView bounds].size.height/2.0f)];
        // Create a circle path.
        float diameter = 10.0f;
        UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:
                              CGRectMake(currentPoint.x -(diameter/2), currentPoint.y - (diameter/2), diameter, diameter)];
        // Set the path on the layer
        [circleLayer setPath:[path CGPath]];
        // Set the stroke color
        [circleLayer setStrokeColor:[[UIColor magentaColor] CGColor]];
        [circleLayer setFillColor:[[UIColor magentaColor] CGColor]];
        // Set the stroke line width
        [circleLayer setLineWidth:2.0f];
        
        // Add the sublayer to the image view's layer tree
        [[_photoView layer] addSublayer:circleLayer];
        
        touchesCounter++;
        if (touchesCounter > 2) {
            touchesCounter = 0;
            
        }
        
        if (ptA.x != 0.0 && ptA.y!= 0.0 && vertex.x != 0.0 && vertex.y != 0.0 && ptB.x != 0.0 && ptB.y != 0.0) {
            [self calculateAngle:ptA vertex:vertex ptb:ptB];
            
        }
        

    }
}
    
-(IBAction)conjugatePressed:(id)sender{
    if (isConjugate) {
        isConjugate = NO;
    }else{
        isConjugate=YES;
    }
    CALayer* layer = self.photoView.layer.sublayers.lastObject;
    [layer removeFromSuperlayer];
    [self calculateAngle:ptA vertex:vertex ptb:ptB];
}

-(void) calculateAngle:(CGPoint)pt1 vertex: (CGPoint)v ptb:(CGPoint)pt2{
    float lenVto1 = sqrt((v.x - pt1.x)*(v.x - pt1.x) + (v.y - pt1.y)*(v.y - pt1.y));
    float lenVto2 = sqrt((v.x - pt2.x)*(v.x - pt2.x) + (v.y - pt2.y)*(v.y - pt2.y));
    float len1to2 = sqrt((pt1.x - pt2.x)*(pt1.x - pt2.x) + (pt1.y - pt2.y)*(pt1.y - pt2.y));
    float angle = acosf((lenVto1*lenVto1 + lenVto2*lenVto2 - len1to2*len1to2)/(2*lenVto1*lenVto2));
    //float chkAngle = atanf((lenVto1*lenVto1 + lenVto2*lenVto2 - len1to2*len1to2)/(2*lenVto1*lenVto2));
    
    float degAngle = angle *180/M_PI;
    
    CAShapeLayer *lineLayer = [CAShapeLayer layer];
    [lineLayer setBounds:CGRectMake(0.0f, 0.0f, [_photoView bounds].size.width,
                                    [_photoView bounds].size.height)];
    [lineLayer setPosition:CGPointMake([_photoView bounds].size.width/2.0f,
                                       [_photoView bounds].size.height/2.0f)];
    UIBezierPath *linePath = [UIBezierPath bezierPath];
    [linePath moveToPoint:pt1];
    [linePath addLineToPoint:v];
    [linePath addLineToPoint:pt2];
    [lineLayer setPath:[linePath CGPath]];
    [lineLayer setStrokeColor:[[UIColor magentaColor] CGColor]];
    [lineLayer setFillColor:[[UIColor clearColor] CGColor]];
    [lineLayer setLineWidth:2.0f];
    [[_photoView layer] addSublayer:lineLayer];

    
    CAShapeLayer *circleLayer = [CAShapeLayer layer];
    // Give the layer the same bounds as your image view
    [circleLayer setBounds:CGRectMake(0.0f, 0.0f, [_photoView bounds].size.width,
                                      [_photoView bounds].size.height)];
    // Position the circle anywhere you like, but this will center it
    // In the parent layer, which will be your image view's root layer
    [circleLayer setPosition:CGPointMake([_photoView bounds].size.width/2.0f,
                                         [_photoView bounds].size.height/2.0f)];
    // Create a circle path.
    float min = MIN(lenVto1, lenVto2);
    CGPoint zeroPt = CGPointMake(v.x + min, v.y);
    //float lenVto1 = sqrt((v.x - pt1.x)*(v.x - pt1.x) + (v.y - pt1.y)*(v.y - pt1.y));
    float lenVto0 = sqrt((v.x - zeroPt.x)*(v.x - zeroPt.x) + (v.y - zeroPt.y)*(v.y - zeroPt.y));
    float len1to0 = sqrt((pt1.x - zeroPt.x)*(pt1.x - zeroPt.x) + (pt1.y - zeroPt.y)*(pt1.y - zeroPt.y));
    float len2to0 = sqrtf((pt2.x - zeroPt.x)*(pt2.x - zeroPt.x) + (pt2.y - zeroPt.y)*(pt2.y - zeroPt.y));
    float startAngle = acosf((lenVto1*lenVto1 + lenVto0*lenVto0 - len1to0*len1to0)/(2*lenVto1*lenVto0));
    float endAngle = acosf((lenVto2*lenVto2 + lenVto0*lenVto0 - len2to0*len2to0)/(2*lenVto2*lenVto0));
    BOOL isCW = isConjugate;
    
    
    if (pt1.y < v.y && pt2.y < v.y) {
        NSLog(@"both ys less");
        startAngle = 2*M_PI - startAngle;
        endAngle = 2*M_PI - endAngle;
        if (pt1.x < v.x) {
            isCW   = !isCW;
        }
    }
    if(pt1.y < v.y && pt2.y >= v.y){
        NSLog(@"1 y is less 2 y is more");
        startAngle = 2*M_PI - startAngle;
        if(pt1.x >= v.x && pt2.x >= v.x){
            isCW   = !isCW;
        }else if(pt1.x <= v.x && pt2.x < v.x){
            //isCW   = !isCW;
        }
        else{
            
            if (M_PI + endAngle < fabs(startAngle)){
                //NSLog(@"start %f, end %f ", startAngle*180/M_PI, endAngle*180/M_PI);
                 isCW   = !isCW;
                
            }else {
               // NSLog(@"start %f, ", startAngle - M_PI);
                
                //isCW   = !isCW;
            }
        }

    }
    if (pt2.y < v.y & pt1.y  >= v.y) {
        NSLog(@"2 y is less 1 y is more");
        endAngle = 2*M_PI - endAngle;
                
        if(pt1.x >= v.x && pt2.x >= v.x){
            //isCW   = !isCW;
        }else if(pt1.x <= v.x && pt2.x < v.x){
            isCW   = !isCW;
        }
        else{
            
            if (M_PI + startAngle < fabs(endAngle)){
                NSLog(@"start %f, end %f ", startAngle*180/M_PI, endAngle*180/M_PI);
               // isCW   = !isCW;

            }else {
                NSLog(@"start %f, ", startAngle - M_PI);

                isCW   = !isCW;
            }
        }
    }
    
    if (pt1.y > v.y && pt2.y > v.y) {
        NSLog(@"both ys more");
        if (pt1.x < v.x) {
            isCW   = !isCW;
        }
         //isCW   = !isCW;
    }
    
    UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:v radius:min/2 startAngle:startAngle endAngle:endAngle clockwise:isCW];
    [circleLayer setPath:[path CGPath]];
    [circleLayer setStrokeColor:[[UIColor magentaColor] CGColor]];
    [circleLayer setFillColor:[[UIColor clearColor] CGColor]];
    [circleLayer setLineWidth:2.0f];
    [[_photoView layer] addSublayer:circleLayer];
    
    
    
   
    [self displayAngleMeasures:angle degrees:degAngle];
    
    
}
-(void) displayAngleMeasures:(float)rad degrees:(float)deg{
    if (isConjugate){
        rad = 2*M_PI - rad;
        deg = 360.0 - deg;
    }
    self.degreeVal.text = [NSString stringWithFormat: @"Angle in Degrees: %.2f", deg];
    self.radVal.text = [NSString stringWithFormat: @"Angle in Radians: %.2f", rad];
    [self.degreeVal setHidden:NO];
    [self.radVal setHidden:NO];
    [self.conjBtn setHidden:NO];
}

-(void) showInfo{
    UIAlertView* info = [[UIAlertView alloc]initWithTitle:@"Protractor Help" message:@"To use the protractor: \n 1. Tap on the camera in the upper right corner to take a picture of the angle you want to measure. \n 2. Click on one of the sides of the angle, then the vertex, and finally on the other side of the angle." delegate:self cancelButtonTitle:@"Got It!" otherButtonTitles: nil];
    [info show];
}
- (void) showCamera
{
    
    self.picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    // Must be NO.
    //self.picker.showsCameraControls = NO;
    self.picker.cameraViewTransform =
    CGAffineTransformScale(self.picker.cameraViewTransform, 1, 1);
    
   // self.picker.cameraOverlayView = overlay;
    [self presentViewController:self.picker animated:YES completion:nil];
    
}
- (void) imagePickerController:(UIImagePickerController *)aPicker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    UIImage *aImage = (UIImage *)[info objectForKey:UIImagePickerControllerOriginalImage];
    [_photoView setImage:aImage];
    
    
    
    
    [self.picker dismissViewControllerAnimated:YES completion:nil];
    
    
    //overlay.pictureButton.enabled = YES;
    //self.picker.cameraDevice = UIImagePickerControllerCameraDeviceRear;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Split view

- (void)splitViewController:(UISplitViewController *)splitController willHideViewController:(UIViewController *)viewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)popoverController
{
    barButtonItem.title = NSLocalizedString(@"Tools", @"Tools");
    barButtonItem.tintColor = [UIColor lightGrayColor];
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
    self.masterPopoverController = popoverController;
}

- (void)splitViewController:(UISplitViewController *)splitController willShowViewController:(UIViewController *)viewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    // Called when the view is shown again in the split view, invalidating the button and popover controller.
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
    self.masterPopoverController = nil;
}


@end
