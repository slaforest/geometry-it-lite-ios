//
//  RightTriangleCheckerViewController.m
//  AlgebralatorUniversal
//
//  Created by Scott LaForest on 4/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RightTriangleCheckerViewController.h"
#import "AppDelegate.h"
#import "DetailViewController.h"

@implementation RightTriangleCheckerViewController;

@synthesize aTextField, bTextField, cTextField, directionsTextView, answerTextView, navigationBar, scrollView, bannerIsVisible, detailViewController, summaryViewController, mainDetailViewController ;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //self.title = NSLocalizedString(@"Tools", @"Tools");
        if ([ADBannerView instancesRespondToSelector:@selector(initWithAdType:)]) {
            _adBannerView = [[ADBannerView alloc] initWithAdType:ADAdTypeBanner];
        } else {
            _adBannerView = [[ADBannerView alloc] init];
        }
        _adBannerView.frame = CGRectMake(0, self.view.frame.size.height, _adBannerView.frame.size.width, _adBannerView.frame.size.height);
        _adBannerView.delegate = self;
    }
    return self;
}


- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    activeField = nil;
}
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        // If active text field is hidden by keyboard, scroll it so it's visible
        // Your application might not need or want this behavior.
        CGRect aRect = self.view.frame;
        aRect.size.height -= kbSize.height;
        if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
            CGPoint scrollPoint = CGPointMake(0.0, kbSize.height - 2.4*activeField.frame.origin.y);//-kbSize.height);
            [scrollView setContentOffset:scrollPoint animated:YES];
        }
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
}

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
 if (self) {
 // Custom initialization.
 }
 return self;
 }
 */

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return YES;
}


# pragma Controlling UI, user input, and keyboard

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

-(IBAction) textFieldDoneEditing:(id)sender{
	
    
	[sender resignFirstResponder];	
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string {
    NSString *currentString = textField.text;
    if ([string isEqualToString:@""]) return YES;
    
    if ([currentString rangeOfString:@"."].location == NSNotFound && [string isEqualToString:@"."]) return YES;
    
    //if (currentString.length == 0 && [string isEqualToString:@"-"]) return YES;
    
    unichar ch = [string characterAtIndex:0];
    if ([[NSCharacterSet decimalDigitCharacterSet] characterIsMember:ch]) {
        return YES;
    } else {
        UIAlertView *badInput = [[UIAlertView alloc] initWithTitle:@"Invalid Input"
                                                           message:@"Please enter in only positive rational number values."
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil];
        [badInput show];
        
        return NO;
    }
    
    
    return YES;
}

-(void)viewWillAppear:(BOOL)animated   {
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"geometryItLite_NoAds"]) {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            if (_adBannerView) {
                [_adBannerView removeFromSuperview];
            }
        }
        else {
            AppDelegate* appDelgate = [[UIApplication sharedApplication]delegate];
            if (appDelgate.window.rootViewController.class != appDelgate.splitViewController.class) {
                [appDelgate setUpMainScreen];
                
                
            }
        }
        
    }
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [self.view addSubview:_adBannerView];
    }
    [self registerForKeyboardNotifications];
    
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        
        [scrollView setContentSize:CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height+150)];
        scrollView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth;
    }
    
    self.aTextField.delegate = self;
    self.bTextField.delegate = self;
    self.cTextField.delegate = self;
    //[[self view]setBackgroundColor:[UIColor darkGrayColor]];
    
    // For the border and rounded corners
    [[answerTextView layer] setBorderColor:[[UIColor magentaColor] CGColor]];
    [[answerTextView layer] setBorderWidth:2.3];
    [[answerTextView layer] setCornerRadius:15];
    [answerTextView setClipsToBounds: YES];
    //NSLog(@"%@", [UIFont fontNamesForFamilyName:@"Futura"]);
    // For the border and rounded corners
    [[directionsTextView layer] setBorderColor:[[UIColor magentaColor] CGColor]];
    [[directionsTextView layer] setBorderWidth:2.3];
    [[directionsTextView  layer] setCornerRadius:15];
    [directionsTextView setClipsToBounds: YES];
    
}



- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.navigationBar = nil;
    
}
-(IBAction)clearButtonPressed:(id)sender{
    
    aTextField.text = @"";
    bTextField.text = @"";
    cTextField.text = @"";
    answerTextView.text = @"";
    /*[self.toolBar setHidden:YES];
     if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
     mainDetailViewController.detailAnswerView.text = @"";
     mainDetailViewController.navigationItem.rightBarButtonItem = nil;
     [mainDetailViewController.solutionWebView loadHTMLString:@"" baseURL:nil];
     }*/
}

- (IBAction) checkButtonPressed: (id) sender{
    
    a = [aTextField.text doubleValue];
    b = [bTextField.text doubleValue];
    c = [cTextField.text doubleValue];
    
    NSLog(@"a string = %g, b string = %g , c String = %g",a, b, c);
    if (c <= a || c <= b){
        UIAlertView *cIsNotBiggest = [[UIAlertView alloc] initWithTitle:@"Invalid Entry!"
                                                                message:@"The c value must be the longest side."
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
        [cIsNotBiggest show];
        
    } else {
        if((a*a + b*b) == c*c){
            NSString *firstLine = [NSString stringWithFormat:@" Since  %g + %g = %g, the triangle is a right triangle.", a*a, b*b,c*c];
            answerTextView.text = firstLine;
        }else {
            NSString *firstLine = [NSString stringWithFormat:@"Since %g + %g ≠ %g, \n the triangle is NOT a right triangle.",a*a,b*b,c*c];
            answerTextView.text = firstLine;
        }
        
              
        
    }
    
}


- (void)layoutAnimated:(BOOL)animated
{
    // As of iOS 6.0, the banner will automatically resize itself based on its width.
    // To support iOS 5.0 however, we continue to set the currentContentSizeIdentifier appropriately.
    CGRect contentFrame = self.view.bounds;
    if (contentFrame.size.width < contentFrame.size.height) {
        //_adBannerView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierPortrait;
    } else {
        // _adBannerView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierLandscape;
    }
    
    CGRect bannerFrame = _adBannerView.frame;
    if (_adBannerView.bannerLoaded) {
        contentFrame.size.height -= _adBannerView.frame.size.height;
        bannerFrame.origin.y = contentFrame.size.height;
    } else {
        bannerFrame.origin.y = contentFrame.size.height;
    }
    
    [UIView animateWithDuration:animated ? 0.25 : 0.0 animations:^{
        self.view.frame = contentFrame;
        [self.view layoutSubviews];
        _adBannerView.frame = bannerFrame;
    }];
}

#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_6_0
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}
#endif

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

- (void)viewDidLayoutSubviews
{
    [self layoutAnimated:[UIView areAnimationsEnabled]];
}

- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    
    [self layoutAnimated:YES];
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    [self layoutAnimated:YES];
}

- (BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave
{
    return YES;
}

- (void)bannerViewActionDidFinish:(ADBannerView *)banner
{
}



@end
