//
//  StoreDetailViewController.m
//  GeomeTryItLite
//
//  Created by Scott LaForest on 7/20/13.
//  Copyright (c) 2013 Zoo Town Tech. All rights reserved.
//

#import "StoreDetailViewController.h"
#import "SimpleTableCell.h"
@interface StoreDetailViewController ()

@end

@implementation StoreDetailViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIView *footerView  = [[UIView alloc] init];
    
    //create the button
    UIButton *addNewBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //the button should be as big as a table view cell
    [addNewBtn setFrame:self.tableView.frame];
    
    //set title, font size and font color
    [addNewBtn setTitle:@"Buy" forState:UIControlStateNormal];
    [addNewBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:20]];
    [addNewBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [addNewBtn addTarget:self action:@selector(buyButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:addNewBtn];
    footerView.userInteractionEnabled = YES;
    self.tableView.tableFooterView = footerView;
    self.tableView.tableFooterView.userInteractionEnabled = YES;


    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(void)buyButtonPressed{
    NSLog(@"name: %@",_product.localizedTitle);
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableCell";
    
    SimpleTableCell *cell = (SimpleTableCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SimpleTableCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    
    
    cell.nameLabel.text = _product.localizedTitle;
    cell.descriptionLabel.text = _product.localizedDescription;
    cell.accessoryType = UITableViewCellAccessoryNone;

    //format currency
    NSLocale* storeLocale = _product.priceLocale;
    NSString *storeCountry = (NSString*)CFLocaleGetValue((CFLocaleRef)storeLocale, kCFLocaleCurrencySymbol);
    NSString* priceStr = [NSString stringWithFormat:@"%@ %@",storeCountry, _product.price];
    int lines = (int) _product.localizedDescription.length/30;
    NSLog(@"lines %d", lines);
    cell.descriptionLabel.numberOfLines = lines + 1 ;
    cell.descriptionLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    cell.priceLabel.text = priceStr;
    //cell.prepTimeLabel.text = [prepTime objectAtIndex:indexPath.row];
    /* static NSString *CellIdentifier = @"Cell";
     UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
     if (cell == nil) {
     cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
     }
     
     // Configure the cell...
     cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
     SKProduct * product = (SKProduct *) _products[indexPath.row];
     
     //format currency
     NSLocale* storeLocale = product.priceLocale;
     NSString *storeCountry = (NSString*)CFLocaleGetValue((CFLocaleRef)storeLocale, kCFLocaleCurrencySymbol);
     NSString* priceStr = [NSString stringWithFormat:@"%@",product.price];
     
     cell.textLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
     cell.textLabel.text = [NSString stringWithFormat: @"%@:  %@ %@",product.localizedTitle, storeCountry, priceStr];
     cell.detailTextLabel.text = product.localizedDescription;
     int lines = (int) product.localizedDescription.length/35;
     cell.detailTextLabel.numberOfLines = lines ;*/
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *str = _product.localizedDescription;
    
    CGSize size = [str sizeWithFont:[UIFont fontWithName:@"Helvetica" size:17] constrainedToSize:CGSizeMake(280, 999) lineBreakMode:NSLineBreakByWordWrapping];
    NSLog(@"%f",size.height);
    if (size.height < 50) {
        size.height = size.height*1.5;
    }
    return size.height ;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

@end
