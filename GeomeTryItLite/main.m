//
//  main.m
//  GeomeTryItLite
//
//  Created by Scott LaForest on 7/15/13.
//  Copyright (c) 2013 Zoo Town Tech. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
