#import <UIKit/UIKit.h>
#import <iAd/iAd.h>

extern NSString * const BannerViewActionWillBegin;
extern NSString * const BannerViewActionDidFinish;

@interface BannerViewController : UIViewController <ADBannerViewDelegate>
@property (strong, nonatomic) ADBannerView * bannerView;
- (instancetype)initWithContentViewController:(UIViewController *)contentController;
-(void)removeBannerView;
@end