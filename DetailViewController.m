//
//  DetailViewController.m
//  GeomeTry-It
//
//  Created by Scott LaForest on 5/7/13.
//  Copyright (c) 2013 Zoo Town Tech. All rights reserved.
//

#import "DetailViewController.h"
#import "AppDelegate.h"

@interface DetailViewController ()
@property (strong, nonatomic) UIPopoverController *masterPopoverController;
- (void)configureView;
@end

@implementation DetailViewController

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem
{
            // Update the view.
        [self configureView];
    

    if (self.masterPopoverController != nil) {
        [self.masterPopoverController dismissPopoverAnimated:YES];
    }        
}

- (void)configureView
{
    // Update the user interface for the detail item.

}
-(void) updateWebview{
    if(_urlstring){
        NSURLRequest* request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:_urlstring]];
        [_webView loadRequest:request];
    }
    
    if(_definition){
        [_webView loadHTMLString:_definition baseURL:nil];
    }

}
- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [self.view addSubview:_adBannerView];
    }
    if(_urlstring){
        NSURLRequest* request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:_urlstring]];
        [_webView loadRequest:request];
    }
    
    if(_definition){
        [_webView loadHTMLString:_definition baseURL:nil];
    }

	// Do any additional setup after loading the view, typically from a nib.
    [self configureView];
}
-(void)viewWillAppear:(BOOL)animated   {
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"geometryItLite_NoAds"]) {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            if (_adBannerView) {
                [_adBannerView removeFromSuperview];
            }
        }
        else {
            AppDelegate* appDelgate = [[UIApplication sharedApplication]delegate];
            if (appDelgate.window.rootViewController.class != appDelgate.splitViewController.class) {
                [appDelgate setUpMainScreen];
                
                
            }
        }
        
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"GeomeTry It", @"GeomeTry It");
        if ([ADBannerView instancesRespondToSelector:@selector(initWithAdType:)]) {
            _adBannerView = [[ADBannerView alloc] initWithAdType:ADAdTypeBanner];
        } else {
            _adBannerView = [[ADBannerView alloc] init];
        }
        _adBannerView.delegate = self;
    }
    return self;
}
#pragma mark - Webview
-(void)webViewDidFinishLoad:(UIWebView *)webView{
    NSLog(@"yep");
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    NSString* errorStr = [error localizedDescription];
    UIAlertView* webError = [[UIAlertView alloc]initWithTitle:errorStr message:@"Check to make sure you have internet access or try again later." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [webError show];
}
#pragma mark - Split view

- (void)splitViewController:(UISplitViewController *)splitController willHideViewController:(UIViewController *)viewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)popoverController
{
    barButtonItem.title = NSLocalizedString(@"Tools", @"Tools");
    barButtonItem.tintColor = [UIColor lightGrayColor];

    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
    self.masterPopoverController = popoverController;
}

- (void)splitViewController:(UISplitViewController *)splitController willShowViewController:(UIViewController *)viewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    // Called when the view is shown again in the split view, invalidating the button and popover controller.
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
    self.masterPopoverController = nil;
}

- (void)layoutAnimated:(BOOL)animated
{
    // As of iOS 6.0, the banner will automatically resize itself based on its width.
    // To support iOS 5.0 however, we continue to set the currentContentSizeIdentifier appropriately.
    CGRect contentFrame = self.view.bounds;
    if (contentFrame.size.width < contentFrame.size.height) {
       // _adBannerView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierPortrait;
    } else {
        //_adBannerView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierLandscape;
    }
    
    CGRect bannerFrame = _adBannerView.frame;
    if (_adBannerView.bannerLoaded) {
        contentFrame.size.height -= _adBannerView.frame.size.height;
        bannerFrame.origin.y = contentFrame.size.height;
    } else {
        bannerFrame.origin.y = contentFrame.size.height;
    }
    
    [UIView animateWithDuration:animated ? 0.25 : 0.0 animations:^{
        _contentView.frame = contentFrame;
        [_contentView layoutIfNeeded];
        _adBannerView.frame = bannerFrame;
    }];
}

#if __IPHONE_OS_VERSION_MIN_REQUIRED < __IPHONE_6_0
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}
#endif

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

- (void)viewDidLayoutSubviews
{
    [self layoutAnimated:[UIView areAnimationsEnabled]];
}

- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    
    [self layoutAnimated:YES];
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    [self layoutAnimated:YES];
}

- (BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave
{
    return YES;
}

- (void)bannerViewActionDidFinish:(ADBannerView *)banner
{
}



@end
