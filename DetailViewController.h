//
//  DetailViewController.h
//  GeomeTry-It
//
//  Created by Scott LaForest on 5/7/13.
//  Copyright (c) 2013 Zoo Town Tech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iAd/iAd.h>


@interface DetailViewController : UIViewController <UISplitViewControllerDelegate, UIWebViewDelegate, UIAlertViewDelegate, ADBannerViewDelegate>
{
    
}

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property(nonatomic, strong) NSString* urlstring;
@property(nonatomic, strong) NSString* definition;
@property (nonatomic, retain) IBOutlet UIView *contentView;
@property (nonatomic, retain) ADBannerView* adBannerView;
@property (nonatomic) BOOL adBannerViewIsVisible;
-(void) updateWebview;
@end
